package controllers

import (
	"context"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func (r *NextcloudReconciler) reconcileService(ctx context.Context, nc nextcloud.Nextcloud) error {
	ls := labelSelectors(nc)
	svc := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      nc.Name,
			Namespace: nc.Namespace,
			Labels:    ls,
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{{
				Name:     httpPortName,
				Protocol: corev1.ProtocolTCP,
				Port:     httpPort,
				TargetPort: intstr.IntOrString{
					Type:   intstr.String,
					StrVal: httpPortName,
				},
			}},
			Selector: ls,
			Type:     corev1.ServiceTypeClusterIP,
		},
	}

	err := r.createOrUpdate(ctx, nc, svc, func() error { return nil })
	if err != nil {
		return err
	}

	return nil
}
