package controllers

import (
	"fmt"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
)

func createConfigVolume(nc nextcloud.Nextcloud) corev1.Volume {
	return createVolume(nc.Name, configVolumeName)
}

func createWebrootVolume(nc nextcloud.Nextcloud) corev1.Volume {
	return createVolume(nc.Name, webrootVolumeName)
}

func createVolume(ncName, volumeName string) corev1.Volume {
	return corev1.Volume{
		Name: volumeName,
		VolumeSource: corev1.VolumeSource{
			PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
				ClaimName: pvcName(ncName, volumeName),
				ReadOnly:  false,
			},
		},
	}
}

func createConfigVolumeMount() corev1.VolumeMount {
	return corev1.VolumeMount{Name: configVolumeName, MountPath: "/var/www/html/config"}
}

func createWebrootVolumeMounts() []corev1.VolumeMount {
	return []corev1.VolumeMount{
		{Name: webrootVolumeName, MountPath: nextcloudWebrootDir, SubPath: nextcloudWebrootDirSubPath},
		{Name: webrootVolumeName, MountPath: nextcloudHtmlDir, SubPath: nextcloudHtmlDirSubPath},
		{Name: webrootVolumeName, MountPath: nextcloudCustomAppsDir, SubPath: nextcloudCustomAppsDirSubPath},
		{Name: webrootVolumeName, MountPath: nextcloudTmpDir, SubPath: nextcloudTmpDirSubPath},
		{Name: webrootVolumeName, MountPath: nextcloudThemesDir, SubPath: nextcloudThemesDirSubPath},
	}
}

func pvcName(ncName, volumeName string) string {
	return fmt.Sprintf("%s-%s", ncName, volumeName)
}

func createNginxConfigVolume(ncName string) corev1.Volume {
	return corev1.Volume{
		Name: nginxConfigVolumeName,
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{Name: nginxConfigmapName(ncName)},
				DefaultMode:          newInt32(420),
				Items: []corev1.KeyToPath{
					{Key: nginxConfKey, Path: nginxConfKey},
				},
			},
		},
	}
}

func createNginxConfigVolumeMount() corev1.VolumeMount {
	return corev1.VolumeMount{
		Name:      nginxConfigVolumeName,
		MountPath: nginxConfigPath,
		SubPath:   nginxConfKey,
	}
}

func createEmptyDirVolume(name, size string) corev1.Volume {
	q := resource.MustParse(size)
	return corev1.Volume{
		Name: name,
		VolumeSource: corev1.VolumeSource{

			EmptyDir: &corev1.EmptyDirVolumeSource{
				SizeLimit: &q,
			},
		},
	}
}
