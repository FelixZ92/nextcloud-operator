package controllers

import (
	"context"
	"fmt"
	"github.com/go-logr/logr"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	batch "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/apiutil"
)

// getDeploymentCondition returns the condition with the provided type.
func getDeploymentCondition(status appsv1.DeploymentStatus,
	condType appsv1.DeploymentConditionType) *appsv1.DeploymentCondition {
	for i := range status.Conditions {
		c := status.Conditions[i]
		if c.Type == condType {
			return &c
		}
	}

	return nil
}

func deploymentProgressFailed(deployment *appsv1.Deployment) (string, bool) {
	// Look for the Progressing condition. If it doesn't exist, we have no base to estimate progress.
	condition := getDeploymentCondition(deployment.Status, appsv1.DeploymentProgressing)
	if condition == nil {
		return "", false
	}

	if condition.Reason == nextcloud.TimedOutReason || condition.Reason == nextcloud.FailedRSCreateReason {
		return condition.Message, true
	}

	return "", false
}

func hasConditionWithStatus(conditions []batch.JobCondition, cond batch.JobConditionType, status corev1.ConditionStatus) bool {
	for _, c := range conditions {
		if c.Type == cond {
			if c.Status == status {
				return true
			}
			return false
		}
	}
	return false
}

func getConditionMessage(conditions []batch.JobCondition, cond batch.JobConditionType) string {
	for _, c := range conditions {
		if c.Type == cond {
			return c.Message
		}
	}
	return ""
}

func (r *NextcloudReconciler) createOrUpdate(ctx context.Context, owner nextcloud.Nextcloud,
	obj client.Object, reconcile func() error) error {

	log := logr.FromContext(ctx)
	runtimeObj, ok := obj.(client.Object)

	if !ok {
		err := fmt.Errorf("%T is not a runtime.Object", obj)
		log.Error(err, "Failed to convert into runtime.Object")

		return err
	}

	if err := ctrl.SetControllerReference(&owner, obj, r.Scheme); err != nil {
		return err
	}

	result, err := ctrl.CreateOrUpdate(ctx, r.Client, runtimeObj, reconcile)
	if err != nil {
		return err
	}

	if gvk, err := apiutil.GVKForObject(runtimeObj, r.Scheme); err != nil {
		log.Info("Reconciled", "Kind", gvk.Kind, "Namespace", obj.GetNamespace(),
			"Name", obj.GetName(), "Status", result)
	}

	return err
}

func labelSelectors(nc nextcloud.Nextcloud) labels.Set {
	return map[string]string{
		nextcloud.NameLabel:           nextcloud.ApplicationName,
		nextcloud.InstanceLabel:       nc.Name,
		nextcloud.ManagedByLabel:      fmt.Sprintf("%s-operator", nextcloud.ApplicationName),
		nextcloud.OwnerLabel:          nc.Name,
		nextcloud.OwnerNamespaceLabel: nc.Namespace,
	}
}

// ObjectKey returns client.ObjectKey for the object.
func ObjectKey(object metav1.Object) client.ObjectKey {
	return client.ObjectKey{
		Namespace: object.GetNamespace(),
		Name:      object.GetName(),
	}
}

func newInt32(i int) *int32 {
	v := int32(i)
	return &v
}

func newInt64(i int) *int64 {
	v := int64(i)
	return &v
}

func newBool(b bool) *bool {
	v := bool(b)
	return &v
}
