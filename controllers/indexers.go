package controllers

import (
	"fmt"
	postgres "github.com/zalando/postgres-operator/pkg/apis/acid.zalan.do/v1"
	appsv1 "k8s.io/api/apps/v1"
	batch "k8s.io/api/batch/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func (r *NextcloudReconciler) indexPostgres(o client.Object) []string {
	// grab the job object, extract the owner...
	pg, ok := o.(*postgres.Postgresql)
	if !ok {
		panic(fmt.Sprintf("Expected a Postgresql, got %T", o))
	}

	owner := metav1.GetControllerOf(pg)
	if owner == nil {
		return nil
	}

	return []string{owner.Name}
}

func (r *NextcloudReconciler) indexJob(o client.Object) []string {
	// grab the job object, extract the owner...
	pg, ok := o.(*batch.Job)
	if !ok {
		panic(fmt.Sprintf("Expected a Job, got %T", o))
	}

	owner := metav1.GetControllerOf(pg)
	if owner == nil {
		return nil
	}

	return []string{owner.Name}
}

func (r *NextcloudReconciler) indexDeployment(o client.Object) []string {
	// grab the job object, extract the owner...
	pg, ok := o.(*appsv1.Deployment)
	if !ok {
		panic(fmt.Sprintf("Expected a Deployment, got %T", o))
	}

	owner := metav1.GetControllerOf(pg)
	if owner == nil {
		return nil
	}

	return []string{owner.Name}
}
