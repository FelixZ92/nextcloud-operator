package controllers

import (
	"context"
	_ "embed"
	"fmt"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

//go:embed files/nginx.conf
var nginxConf string

func (r *NextcloudReconciler) reconcileNginxConfig(ctx context.Context, nc nextcloud.Nextcloud) error {
	ls := labelSelectors(nc)
	cm := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      nginxConfigmapName(nc.Name),
			Namespace: nc.Namespace,
			Labels:    ls,
		},
	}

	if err := r.createOrUpdate(ctx, nc, cm, func() error {
		cm.Data = map[string]string{nginxConfKey: nginxConf}
		return nil
	}); err != nil {
		return err
	}

	return nil
}

func nginxConfigmapName(ncName string) string {
	return fmt.Sprintf("%s%s", ncName, nginxConfigSuffix)
}
