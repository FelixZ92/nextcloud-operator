package controllers

import (
	"fmt"
	"github.com/google/go-cmp/cmp"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func determineResourceRequirements(nc nextcloud.Nextcloud, defaultRequirements corev1.ResourceRequirements) corev1.ResourceRequirements {
	emptyReqs := &corev1.ResourceRequirements{}

	var resourceRequirements corev1.ResourceRequirements

	if cmp.Equal(&nc.Spec.Resources, emptyReqs) {
		resourceRequirements = defaultRequirements
	} else {
		resourceRequirements = nc.Spec.Resources
	}

	return resourceRequirements
}

func defaultResources(cpuRequests, cpuLimits, memoryRequests, memoryLimits string) corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Limits: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse(cpuLimits),
			corev1.ResourceMemory: resource.MustParse(memoryLimits),
		},
		Requests: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse(cpuRequests),
			corev1.ResourceMemory: resource.MustParse(memoryRequests),
		},
	}
}

func defaultFpmResources() corev1.ResourceRequirements {
	return defaultResources(
		fpmDefaultCPURequests,
		fpmDefaultCPULimits,
		fpmDefaultMemoryRequests,
		fpmDefaultMemoryLimits,
	)
}

func defaultNginxResources() corev1.ResourceRequirements {
	return defaultResources(
		nginxDefaultCPURequests,
		nginxDefaultCPULimits,
		nginxDefaultMemoryRequests,
		nginxDefaultMemoryLimits,
	)
}

func createInstallationContainer(nc nextcloud.Nextcloud) corev1.Container {
	env := nextcloudEnvironment(nc)
	env = append(env, corev1.EnvVar{Name: "NEXTCLOUD_UPDATE", Value: "1"})
	return createFpmContainer(nc, env, nc.Spec.InstallationImage)
}

func createFpmContainer(nc nextcloud.Nextcloud, env []corev1.EnvVar, image string) corev1.Container {
	return corev1.Container{
		Name:            nextcloud.ApplicationName,
		Image:           image,
		ImagePullPolicy: corev1.PullAlways,
		Env:             env,
		Resources:       determineResourceRequirements(nc, defaultFpmResources()),
		VolumeMounts:    append(createWebrootVolumeMounts(), createConfigVolumeMount()),
		Ports:           []corev1.ContainerPort{{Name: fpmPortName, ContainerPort: fpmPort, Protocol: corev1.ProtocolTCP}},
		//Args:            []string{arg},
		//Args:    []string{"6000"},
		//Command: []string{"sleep"},
	}
}

func createRuntimeContainer(nc nextcloud.Nextcloud) corev1.Container {
	env := nextcloudEnvironment(nc)
	image := nc.Spec.RuntimeImage
	return createFpmContainer(nc, env, image)
}

func createNginxContainer(nc nextcloud.Nextcloud) corev1.Container {
	image := nc.Spec.NginxSpec.Image
	if image == "" {
		image = defaultNginxImage
	}
	return corev1.Container{
		Name:      fmt.Sprintf("%s%s", nextcloud.ApplicationName, nginxSuffix),
		Image:     image,
		Resources: determineResourceRequirements(nc, defaultNginxResources()),
		VolumeMounts: append(createWebrootVolumeMounts(), createNginxConfigVolumeMount(),
			corev1.VolumeMount{Name: "nginx", MountPath: "/docker-entrypoint.d", SubPath: "docker"},
			corev1.VolumeMount{Name: "nginx", MountPath: "/var/cache/nginx", SubPath: "cache"},
		),
		Ports:          []corev1.ContainerPort{{Name: httpPortName, ContainerPort: httpPort, Protocol: corev1.ProtocolTCP}},
		LivenessProbe:  createNginxProbe(nc),
		ReadinessProbe: createNginxProbe(nc),
	}
}

func createNginxProbe(nc nextcloud.Nextcloud) *corev1.Probe {
	return &corev1.Probe{
		InitialDelaySeconds: 10,
		TimeoutSeconds:      5,
		PeriodSeconds:       5,
		SuccessThreshold:    1,
		FailureThreshold:    3,
		Handler: corev1.Handler{
			HTTPGet: &corev1.HTTPGetAction{
				Path:        nextcloudStatusResource,
				Port:        intstr.IntOrString{Type: intstr.String, StrVal: httpPortName},
				HTTPHeaders: []corev1.HTTPHeader{{Name: hostHeader, Value: nc.Spec.Ingress.Host}},
			},
		},
	}
}
