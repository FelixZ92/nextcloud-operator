package controllers

import (
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"strconv"
	"strings"
)

func nextcloudEnvironment(nc nextcloud.Nextcloud) []corev1.EnvVar {
	env := generalEnvironment(nc)
	env = append(env, postgresEnvironment(nc)...)
	env = append(env, swiftObjectStoreEnvironment(nc)...)
	return env
}

func generalEnvironment(nc nextcloud.Nextcloud) []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name:      NextcloudAdminUser,
			ValueFrom: &corev1.EnvVarSource{SecretKeyRef: nc.Spec.AdminUser.Username},
		},
		{
			Name:      NextcloudAdminPassword,
			ValueFrom: &corev1.EnvVarSource{SecretKeyRef: nc.Spec.AdminUser.Password},
		},
		{
			Name:  NextcloudTrustedDomains,
			Value: strings.Join([]string{nc.Spec.Ingress.Host, nc.Name}, ","),
		},
		{
			Name:  NextcloudDataDir,
			Value: nc.Spec.DataDir,
		},
	}
}

func postgresEnvironment(nc nextcloud.Nextcloud) []corev1.EnvVar {
	if !nc.Spec.Database.Enabled {
		return []corev1.EnvVar{}
	}
	return []corev1.EnvVar{
		{Name: PostgresDb, Value: databaseName},
		{
			Name: PostgresUser,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					Key:                  usernameSecretKey,
					LocalObjectReference: corev1.LocalObjectReference{Name: secretName(databaseUser(nc.Name), nc.Name)},
				},
			},
		},
		{
			Name: PostgresPassword,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					Key:                  passwordSecretKey,
					LocalObjectReference: corev1.LocalObjectReference{Name: secretName(databaseUser(nc.Name), nc.Name)},
				},
			},
		},
		{Name: PostgresHost, Value: databaseServiceName(nc.Name)},
	}
}

func swiftObjectStoreEnvironment(nc nextcloud.Nextcloud) []corev1.EnvVar {
	if nc.Spec.ObjectStorage.SwiftObjectStorage.ContainerName == "" {
		return []corev1.EnvVar{}
	}
	return []corev1.EnvVar{
		{
			Name:  ObjectStoreSwiftUrl,
			Value: nc.Spec.ObjectStorage.SwiftObjectStorage.Url,
		},
		{
			Name:  ObjectStoreSwiftAutocreate,
			Value: strconv.FormatBool(nc.Spec.ObjectStorage.SwiftObjectStorage.Autocreate),
		},
		{
			Name:      ObjectStoreSwiftUserName,
			ValueFrom: &corev1.EnvVarSource{SecretKeyRef: nc.Spec.ObjectStorage.SwiftObjectStorage.UserName},
		},
		{
			Name:      ObjectStoreSwiftUserPassword,
			ValueFrom: &corev1.EnvVarSource{SecretKeyRef: nc.Spec.ObjectStorage.SwiftObjectStorage.UserPassword},
		},
		{
			Name:  ObjectStoreSwiftUserDomain,
			Value: nc.Spec.ObjectStorage.SwiftObjectStorage.UserDomain,
		},
		{
			Name:      ObjectStoreSwiftProjectName,
			ValueFrom: &corev1.EnvVarSource{SecretKeyRef: nc.Spec.ObjectStorage.SwiftObjectStorage.ProjectName},
		},
		{
			Name:  ObjectStoreSwiftProjectDomain,
			Value: nc.Spec.ObjectStorage.SwiftObjectStorage.ProjectDomain,
		},
		{
			Name:  ObjectStoreSwiftServiceName,
			Value: nc.Spec.ObjectStorage.SwiftObjectStorage.ServiceName,
		},
		{
			Name:  ObjectStoreSwiftRegion,
			Value: nc.Spec.ObjectStorage.SwiftObjectStorage.Region,
		},
		{
			Name:  ObjectStoreSwiftContainerName,
			Value: nc.Spec.ObjectStorage.SwiftObjectStorage.ContainerName,
		},
	}
}
