package controllers

import nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"

const (
	installResourceSuffix  = "-install"
	installClusterRoleName = nextcloud.ApplicationName + installResourceSuffix
)

const (
	nginxConfKey = "nginx.conf"
)

const (
	nginxSuffix   = "-nginx"
	configSuffix  = "-config"
	dataSuffix    = "-data"
	webrootSuffix = "-webroot"
)

const (
	dataVolumeName        = nextcloud.ApplicationName + dataSuffix
	webrootVolumeName     = nextcloud.ApplicationName + webrootSuffix
	configVolumeName      = nextcloud.ApplicationName + configSuffix
	nginxConfigSuffix     = nginxSuffix + configSuffix
	nginxConfigVolumeName = nextcloud.ApplicationName + nginxConfigSuffix

	nextcloudWebrootDir        = "/var/www/"
	nextcloudWebrootDirSubPath = "root"
	nextcloudDataDirSubPath    = "data"

	nextcloudHtmlDirSubPath       = "html"
	nextcloudHtmlDir              = nextcloudWebrootDir + nextcloudHtmlDirSubPath
	nextcloudCustomAppsDirSubPath = "custom_apps"
	nextcloudCustomAppsDir        = nextcloudHtmlDir + "/" + nextcloudCustomAppsDirSubPath
	nextcloudTmpDirSubPath        = "tmp"
	nextcloudTmpDir               = nextcloudWebrootDir + nextcloudTmpDirSubPath
	nextcloudThemesDirSubPath     = "themes"
	nextcloudThemesDir            = nextcloudHtmlDir + "/" + nextcloudThemesDirSubPath

	nginxPath       = "/etc/nginx"
	nginxConfigPath = nginxPath + "/" + nginxConfKey

	defaultNginxImage = "nginx:alpine"
)

const (
	fpmDefaultCPULimits        = "200m"
	fpmDefaultCPURequests      = "100m"
	fpmDefaultMemoryLimits     = "512Mi"
	fpmDefaultMemoryRequests   = "256Mi"
	nginxDefaultCPULimits      = "100m"
	nginxDefaultCPURequests    = "50m"
	nginxDefaultMemoryLimits   = "256Mi"
	nginxDefaultMemoryRequests = "128Mi"

	fpmPortName  = "fpm"
	httpPortName = "http"

	fpmPort  = 9000
	httpPort = 8080

	nextcloudStatusResource = "/status.php"
	hostHeader              = "Host"
)

const (
	postgresUser  = "postgres"
	superuser     = nextcloud.ApplicationName
	superuserFlag = "superuser"
	createdbFlag  = "createdb"
	databaseName  = nextcloud.ApplicationName

	usernameSecretKey = "username"
	passwordSecretKey = "password"

	pgVersion = "12"

	exporterName = "exporter"

	exporterDataSourceURIEnv  = "DATA_SOURCE_URI"
	exporterDataSourceUserEnv = "DATA_SOURCE_USER"
	exporterDataSourcePassEnv = "DATA_SOURCE_PASS"

	exporterDataSourceURI = "localhost/nextcloud?sslmode=disable"

	postgresTeamLabel = "team"

	spiloRunAsGroup = 101
	spiloRunAsUser  = 103
	spiloFSGroup    = 103
)

const (
	progressDeadLineSeconds = 180
	revisionHistoryLimit    = 5

	nextcloudFSGroup = 82
)

const (
	// NextcloudAdminUser Name of the Nextcloud admin user.
	NextcloudAdminUser = "NEXTCLOUD_ADMIN_USER"
	// NextcloudAdminPassword Password for the Nextcloud admin user.
	NextcloudAdminPassword = "NEXTCLOUD_ADMIN_PASSWORD"
	// NextcloudDataDir (default: /nextcloud-data) Configures the data directory where nextcloud stores all files from the users.
	NextcloudDataDir = "NEXTCLOUD_DATA_DIR"
	// NextcloudTrustedDomains (not set by default) Optional space-separated list of domains
	NextcloudTrustedDomains = "NEXTCLOUD_TRUSTED_DOMAINS"

	// PostgresDb Name of the database using postgres.
	PostgresDb = "POSTGRES_DB"
	// PostgresUser Username for the database using postgres.
	PostgresUser = "POSTGRES_USER"
	// PostgresPassword Password for the database user using postgres.
	PostgresPassword = "POSTGRES_PASSWORD"
	// PostgresHost Hostname of the database server using postgres.
	PostgresHost = "POSTGRES_HOST"

	// MysqlDatabase Name of the database using mysql / mariadb.
	MysqlDatabase = "MYSQL_DATABASE"
	// MysqlUser Username for the database using mysql / mariadb.
	MysqlUser = "MYSQL_USER"
	// MysqlPassword Password for the database user using mysql / mariadb.
	MysqlPassword = "MYSQL_PASSWORD"
	// MysqlHost Hostname of the database server using mysql / mariadb.
	MysqlHost = "MYSQL_HOST"

	// RedisHost (not set by default) Name of Redis container
	RedisHost = "REDIS_HOST"
	// RedisHostPort (default: 6379) Optional port for Redis, only use for external Redis servers that run on non-standard ports.
	RedisHostPort = "REDIS_HOST_PORT"
	// RedisHostPassword (not set by default) Redis password
	RedisHostPassword = "REDIS_HOST_PASSWORD"

	// SmtpHost (not set by default): The hostname of the SMTP server.
	SmtpHost = "SMTP_HOST"
	// SmtpSecure (empty by default): Set to ssl to use SSL, or tls to use STARTTLS.
	SmtpSecure = "SMTP_SECURE"
	// SmtpPort (default: 465 for SSL and 25 for non-secure connections): Optional port for the SMTP connection. Use 587 for an alternative port for STARTTLS.
	SmtpPort = "SMTP_PORT"
	// SmtpAuthtype (default: LOGIN): The method used for authentication. Use PLAIN if no authentication is required.
	SmtpAuthtype = "SMTP_AUTHTYPE"
	// SmtpName (empty by default): The username for the authentication.
	SmtpName = "SMTP_NAME"
	// SmtpPassword (empty by default): The password for the authentication.
	SmtpPassword = "SMTP_PASSWORD"
	// MailFromAddress (not set by default): Use this address for the 'from' field in the emails sent by Nextcloud.
	MailFromAddress = "MAIL_FROM_ADDRESS"
	// MailDomain (not set by default): Set a different domain for the emails than the domain where Nextcloud is installed.
	MailDomain = "MAIL_DOMAIN"

	// ObjectStoreSwiftUrl : The Swift identity (Keystone) endpoint
	ObjectStoreSwiftUrl = "OBJECTSTORE_SWIFT_URL"
	// ObjectStoreSwiftAutocreate (default: false): Whether or not Nextcloud should automatically create the Swift container
	ObjectStoreSwiftAutocreate = "OBJECTSTORE_SWIFT_AUTOCREATE"
	// ObjectStoreSwiftUserName : Swift username
	ObjectStoreSwiftUserName = "OBJECTSTORE_SWIFT_USER_NAME"
	// ObjectStoreSwiftUserPassword : Swift user password
	ObjectStoreSwiftUserPassword = "OBJECTSTORE_SWIFT_USER_PASSWORD"
	// ObjectStoreSwiftUserDomain (default: Default): Swift user domain
	ObjectStoreSwiftUserDomain = "OBJECTSTORE_SWIFT_USER_DOMAIN"
	// ObjectStoreSwiftProjectName : OpenStack project name
	ObjectStoreSwiftProjectName = "OBJECTSTORE_SWIFT_PROJECT_NAME"
	// ObjectStoreSwiftProjectDomain (default: Default): OpenStack project domain
	ObjectStoreSwiftProjectDomain = "OBJECTSTORE_SWIFT_PROJECT_DOMAIN"
	// ObjectStoreSwiftServiceName (default: swift): Swift service name
	ObjectStoreSwiftServiceName = "OBJECTSTORE_SWIFT_SERVICE_NAME"
	// ObjectStoreSwiftRegion : Swift endpoint region
	ObjectStoreSwiftRegion = "OBJECTSTORE_SWIFT_REGION"
	// ObjectStoreSwiftContainerName : Swift container (bucket) that Nextcloud should store the data in
	ObjectStoreSwiftContainerName = "OBJECTSTORE_SWIFT_CONTAINER_NAME"

	// ObjectStoreS3Host The hostname of the object storage server
	ObjectStoreS3Host = "OBJECTSTORE_S3_HOST"
	// ObjectStoreS3Bucket The name of the bucket that Nextcloud should store the data in
	ObjectStoreS3Bucket = "OBJECTSTORE_S3_BUCKET"
	// ObjectStoreS3Key AWS style access key
	ObjectStoreS3Key = "OBJECTSTORE_S3_KEY"
	// ObjectStoreS3Secret AWS style secret access key
	ObjectStoreS3Secret = "OBJECTSTORE_S3_SECRET"
	// ObjectStoreS3Port The port that the object storage server is being served over
	ObjectStoreS3Port = "OBJECTSTORE_S3_PORT"
	// ObjectStoreS3Ssl (default: true): Whether or not SSL/TLS should be used to communicate with object storage server
	ObjectStoreS3Ssl = "OBJECTSTORE_S3_SSL"
	// ObjectStoreS3Region The region that the S3 bucket resides in.
	ObjectStoreS3Region = "OBJECTSTORE_S3_REGION"
	// ObjectStoreS3UsepathStyle (default: false): Not required for AWS S3
	ObjectStoreS3UsepathStyle = "OBJECTSTORE_S3_USEPATH_STYLE"
	// ObjectStoreS3Legacyauth (default: false): Not required for AWS S3
	ObjectStoreS3Legacyauth = "OBJECTSTORE_S3_LEGACYAUTH"
	// ObjectStoreS3ObjectPrefix (default: urn:oid:): Prefix to prepend to the fileid
	ObjectStoreS3ObjectPrefix = "OBJECTSTORE_S3_OBJECT_PREFIX"
	// ObjectStoreS3Autocreate (default: true): Create the container if it does not exist
	ObjectStoreS3Autocreate = "OBJECTSTORE_S3_AUTOCREATE"
)

const (
	certSuffix       = "-tls-certificate"
	certSecretSuffix = "-nextcloud-ingress"

	middlewareNameRedirectHttps = "redirect-https"
	middlewareNameCaldav        = "caldav"
	middlewareNameWebfinger     = "webfinger"
	middlewareNameHostMeta      = "host-meta"
	middlewareNameHostMetaJson  = "host-meta-json"
)
