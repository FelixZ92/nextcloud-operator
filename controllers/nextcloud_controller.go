/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"github.com/go-logr/logr"
	postgres "github.com/zalando/postgres-operator/pkg/apis/acid.zalan.do/v1"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	batch "k8s.io/api/batch/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"time"
)

const (
	ownerKey = ".metadata.controller"
)

var (
	apiGVStr = nextcloud.GroupVersion.String()
)

// NextcloudReconciler reconciles a Nextcloud object
type NextcloudReconciler struct {
	client.Client
	Log               logr.Logger
	Scheme            *runtime.Scheme
	ReconcileInterval time.Duration
}

// SetupWithManager sets up the controller with the Manager.
func (r *NextcloudReconciler) SetupWithManager(mgr ctrl.Manager) error {

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &postgres.Postgresql{}, ownerKey, r.indexPostgres); err != nil {
		return fmt.Errorf("failed setting index fields: %w", err)
	}

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &batch.Job{}, ownerKey, r.indexJob); err != nil {
		return fmt.Errorf("failed setting index fields: %w", err)
	}

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &appsv1.Deployment{}, ownerKey, r.indexDeployment); err != nil {
		return fmt.Errorf("failed setting index fields: %w", err)
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&nextcloud.Nextcloud{}).
		Owns(&postgres.Postgresql{}).
		Owns(&batch.Job{}).
		Owns(&appsv1.Deployment{}).
		Complete(r)
}

//+kubebuilder:rbac:groups=nextcloud.zippelf.com.zippelf.com,resources=nextclouds,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=nextcloud.zippelf.com.zippelf.com,resources=nextclouds/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=nextcloud.zippelf.com.zippelf.com,resources=nextclouds/finalizers,verbs=update
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs/status,verbs=get

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
func (r *NextcloudReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logr.FromContext(ctx)

	var nc nextcloud.Nextcloud
	if err := r.Get(ctx, req.NamespacedName, &nc); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	//// Add our finalizer if it does not exist
	//if !controllerutil.ContainsFinalizer(&nc, nextcloud.NextcloudFinalizer) {
	//	controllerutil.AddFinalizer(&nc, nextcloud.NextcloudFinalizer)
	//	if err := r.Update(ctx, &nc); err != nil {
	//		log.Error(err, "unable to register finalizer")
	//		return ctrl.Result{}, err
	//	}
	//}

	if !nc.DeletionTimestamp.IsZero() {
		return r.reconcileDelete(ctx, req.NamespacedName)
	}

	reconciledNextcloud, reconcileErr := r.reconcile(ctx, req, *nc.DeepCopy())
	if err := r.patchStatus(ctx, req, reconciledNextcloud.Status); err != nil {
		log.Error(err, "Unable to update status after reconciliation")
		return ctrl.Result{Requeue: true}, err
	}

	if reconcileErr != nil {
		//log.Error(reconcileErr, fmt.Sprintf("Reconciliation failed next try in %s", time.Second*10))
		return ctrl.Result{RequeueAfter: time.Second * 1}, reconcileErr
	}

	return ctrl.Result{RequeueAfter: r.ReconcileInterval}, nil
}

func namespacedName(nc nextcloud.Nextcloud) types.NamespacedName {
	return types.NamespacedName{Name: nc.Name, Namespace: nc.Namespace}
}

func (r *NextcloudReconciler) reconcile(
	ctx context.Context, req ctrl.Request, nc nextcloud.Nextcloud,
) (nextcloud.Nextcloud, error) {

	if nc.DatabaseNotReady() {
		return r.reconcileDatabase(ctx, nc)
	}

	if nc.IsNotInstalled() {
		return r.reconcileInstallation(ctx, req, nc)
	}

	return r.reconcileNextcloud(ctx, req, nc)
}

func (r *NextcloudReconciler) patchStatus(ctx context.Context, req ctrl.Request,
	newStatus nextcloud.NextcloudStatus) error {
	var nc nextcloud.Nextcloud
	if err := r.Get(ctx, req.NamespacedName, &nc); err != nil {
		return err
	}

	patch := client.MergeFrom(nc.DeepCopy())
	nc.Status = newStatus

	return r.Status().Patch(ctx, &nc, patch)
}

func (r *NextcloudReconciler) reconcileNextcloud(ctx context.Context, req ctrl.Request,
	nc nextcloud.Nextcloud) (nextcloud.Nextcloud, error) {

	log := logr.FromContext(ctx)

	if err := r.reconcileNginxConfig(ctx, nc); err != nil {
		return nextcloud.NextcloudNotHealthy(nc, nextcloud.ConfigMapFailedReason, err.Error()), err
	}

	deployment, err := r.reconcileDeployment(ctx, nc)
	if err != nil {
		return nextcloud.NextcloudNotHealthy(nc, nextcloud.DeploymentFailedReason, err.Error()), err
	}

	if err = r.reconcileService(ctx, nc); err != nil {
		return nextcloud.NextcloudNotHealthy(nc, nextcloud.ServiceFailedReason, err.Error()), err
	}

	if err = r.reconcileIngress(ctx, nc); err != nil {
		return nextcloud.NextcloudNotHealthy(nc, nextcloud.IngressFailedReason, err.Error()), err
	}

	selector, err := metav1.LabelSelectorAsSelector(deployment.Spec.Selector)
	if err != nil {
		log.Error(err, "unable to convert labelselector to selector", "selector", selector)

		return nextcloud.NextcloudNotHealthy(nc,
				nextcloud.DeploymentFailedReason,
				err.Error()),
			err
	}

	return r.updateStatus(ctx, nc, selector, deployment.Status.Replicas)

}

func (r *NextcloudReconciler) reconcileDelete(ctx context.Context, name types.NamespacedName) (ctrl.Result, error) {
	return ctrl.Result{}, nil
}

func (r *NextcloudReconciler) updateStatus(ctx context.Context, nc nextcloud.Nextcloud,
	selector labels.Selector, replicas int32) (nextcloud.Nextcloud, error) {
	nc.Status.Selector = selector.String()
	nc.Status.Replicas = replicas

	var deployment appsv1.Deployment
	if err := r.Get(ctx, types.NamespacedName{
		Name:      nc.Name,
		Namespace: nc.Namespace,
	}, &deployment); err != nil {
		return nextcloud.NextcloudInstallFailed(nc, nextcloud.HealthCheckFailedReason, err.Error()), nil
	}

	err := r.checkHealth(deployment)
	if err != nil {
		return nextcloud.NextcloudNotHealthy(nc,
				nextcloud.HealthCheckFailedReason,
				err.Error()),
			nil
	}

	progressing := r.checkProgressing(deployment, nc)

	if progressing {
		return nextcloud.NextcloudInstalling(nc), nil
	}

	return nextcloud.NextcloudHealthy(nc), nil
}
