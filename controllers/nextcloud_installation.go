package controllers

import (
	"context"
	"fmt"
	"github.com/go-logr/logr"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	batch "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	rbac "k8s.io/api/rbac/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *NextcloudReconciler) reconcileInstallation(ctx context.Context, req ctrl.Request, nc nextcloud.Nextcloud) (nextcloud.Nextcloud, error) {
	log := logr.FromContext(ctx)

	if err := r.reconcilePrerequisites(ctx, nc); err != nil {
		return nextcloud.NextcloudInstallFailed(nc, nextcloud.InstallationFailedReason, err.Error()), err
	}

	var installJob batch.Job
	if err := r.Get(ctx, types.NamespacedName{Name: installationResourceName(nc.Name), Namespace: nc.Namespace}, &installJob); err != nil {
		if apierrors.IsNotFound(err) {
			log.Info("create installation job")
			return r.createInstallJob(ctx, nc)
		}
		return nextcloud.NextcloudInstallFailed(nc, nextcloud.InstallationFailedReason, err.Error()), err
	}

	if hasConditionWithStatus(installJob.Status.Conditions, batch.JobComplete, corev1.ConditionTrue) {
		return nextcloud.NextcloudInstalled(
			nc,
			nextcloud.InstallationSucceededReason,
			"Install job succeeded",
		), nil
	}

	if hasConditionWithStatus(installJob.Status.Conditions, batch.JobFailed, corev1.ConditionTrue) {
		return nextcloud.NextcloudInstallFailed(
			nc,
			nextcloud.InstallationFailedReason,
			getConditionMessage(installJob.Status.Conditions, batch.JobFailed),
		), nil
	}

	return nextcloud.NextcloudInstalling(nc), nil
}

func (r *NextcloudReconciler) reconcilePrerequisites(ctx context.Context, nc nextcloud.Nextcloud) error {
	if err := r.reconcileServiceAccount(ctx, nc); err != nil {
		return err
	}

	if err := r.reconcileInstallRoleBinding(ctx, nc); err != nil {
		return err
	}

	if err := r.reconcileWebrootClaim(ctx, nc); err != nil {
		return err
	}

	if err := r.reconcileConfigClaim(ctx, nc); err != nil {
		return err
	}

	return nil
}

func (r *NextcloudReconciler) createInstallJob(ctx context.Context, nc nextcloud.Nextcloud) (nextcloud.Nextcloud, error) {

	job := &batch.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      installationResourceName(nc.Name),
			Namespace: nc.Namespace,
			Labels:    labelSelectors(nc),
		},
		Spec: batch.JobSpec{
			BackoffLimit:            newInt32(3),
			TTLSecondsAfterFinished: newInt32(300),
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					RestartPolicy:      corev1.RestartPolicyOnFailure,
					ServiceAccountName: installationResourceName(nc.Name),
					Containers: []corev1.Container{
						createInstallationContainer(nc),
					},
					Volumes: []corev1.Volume{
						createWebrootVolume(nc),
						createConfigVolume(nc),
						createEmptyDirVolume(dataVolumeName, "100Mi"),
					},
					SecurityContext: &corev1.PodSecurityContext{
						RunAsNonRoot: newBool(true),
						FSGroup:      newInt64(nextcloudFSGroup),
						RunAsUser:    newInt64(nextcloudFSGroup),
						RunAsGroup:   newInt64(nextcloudFSGroup),
					},
				},
			},
		},
	}

	if err := ctrl.SetControllerReference(&nc, job, r.Scheme); err != nil {
		return nextcloud.NextcloudInstalled(nc, nextcloud.InstallationFailedReason, err.Error()), err
	}

	if err := r.Create(ctx, job); err != nil {
		return nextcloud.NextcloudInstallFailed(nc, nextcloud.InstallationFailedReason, err.Error()), err
	}

	return nextcloud.NextcloudInstalling(nc), nil
}

func (r *NextcloudReconciler) reconcileServiceAccount(ctx context.Context, nc nextcloud.Nextcloud) error {
	serviceAccount := &corev1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      installationResourceName(nc.Name),
			Namespace: nc.Namespace,
		},
	}

	return r.createOrUpdate(ctx, nc, serviceAccount, func() error { return nil })
}

func (r *NextcloudReconciler) reconcileConfigClaim(ctx context.Context, nc nextcloud.Nextcloud) error {
	return r.reconcilePersistenceVolumeClaim(ctx, nc.Spec.Persistence.WebrootVolume, nc, configVolumeName)
}

func (r *NextcloudReconciler) reconcileWebrootClaim(ctx context.Context, nc nextcloud.Nextcloud) error {
	return r.reconcilePersistenceVolumeClaim(ctx, nc.Spec.Persistence.WebrootVolume, nc, webrootVolumeName)
}

func (r *NextcloudReconciler) reconcilePersistenceVolumeClaim(ctx context.Context,
	spec corev1.PersistentVolumeClaimSpec, owner nextcloud.Nextcloud, volumeName string) error {

	claimName := pvcName(owner.Name, volumeName)
	key := types.NamespacedName{Name: claimName, Namespace: owner.Namespace}
	var pvc corev1.PersistentVolumeClaim
	if err := r.Get(ctx, key, &pvc); err != nil {
		if apierrors.IsNotFound(err) {
			pvc := &corev1.PersistentVolumeClaim{
				ObjectMeta: metav1.ObjectMeta{
					Name:      claimName,
					Namespace: owner.Namespace,
				},
				Spec: spec,
			}
			if err = ctrl.SetControllerReference(&owner, pvc, r.Scheme); err != nil {
				return err
			}
			if err = r.Create(ctx, pvc); err != nil {
				return err
			}
			return nil
		}
		return err
	}

	return nil
}

func (r *NextcloudReconciler) reconcileInstallRoleBinding(ctx context.Context, nc nextcloud.Nextcloud) error {
	serviceAccount := &rbac.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      installationResourceName(nc.Name),
			Namespace: nc.Namespace,
		},
		RoleRef: rbac.RoleRef{
			APIGroup: rbac.GroupName,
			Kind:     "ClusterRole",
			Name:     installClusterRoleName,
		},
		Subjects: []rbac.Subject{
			{
				Kind:      "ServiceAccount",
				Name:      installationResourceName(nc.Name),
				Namespace: nc.Namespace,
			},
		},
	}

	return r.createOrUpdate(ctx, nc, serviceAccount, func() error { return nil })
}

func installationResourceName(ncName string) string {
	return fmt.Sprintf("%s%s", ncName, installResourceSuffix)
}
