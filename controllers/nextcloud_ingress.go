package controllers

import (
	"context"
	"fmt"
	certmanager "github.com/jetstack/cert-manager/pkg/apis/certmanager/v1alpha2"
	traefikdynamic "github.com/traefik/traefik/v2/pkg/config/dynamic"
	traefik "github.com/traefik/traefik/v2/pkg/provider/kubernetes/crd/traefik/v1alpha1"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (r *NextcloudReconciler) reconcileIngress(ctx context.Context, nc nextcloud.Nextcloud) error {

	if err := r.reconcileCert(ctx, nc); err != nil {
		return err
	}

	if err := r.reconcileMiddlewares(ctx, nc); err != nil {
		return err
	}

	return r.reconcileIngressRoute(ctx, nc)
}

func (r *NextcloudReconciler) reconcileCert(ctx context.Context, nc nextcloud.Nextcloud) error {
	cert := &certmanager.Certificate{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s%s", nc.Name, certSuffix),
			Namespace: nc.Namespace,
			Labels:    labelSelectors(nc),
		},
		Spec: certmanager.CertificateSpec{
			SecretName: certSecretName(nc.Name),
			IssuerRef:  nc.Spec.Ingress.Certificate.Issuer,
			DNSNames:   []string{nc.Spec.Ingress.Host},
		},
	}

	return r.createOrUpdate(ctx, nc, cert, func() error { return nil })
}

func (r *NextcloudReconciler) reconcileMiddlewares(ctx context.Context, nc nextcloud.Nextcloud) error {

	if err := r.reconcileRedirectHttpsMiddleware(ctx, nc); err != nil {
		return err
	}

	if err := r.reconcileReplaceRegexMiddleware(
		ctx, nc, middlewareName(nc.Name, middlewareNameCaldav),
		"^/.well-known/ca(l|rd)dav", "/remote.php/dav/",
	); err != nil {
		return err
	}

	if err := r.reconcileReplaceRegexMiddleware(
		ctx, nc, middlewareName(nc.Name, middlewareNameWebfinger),
		"^/.well-known/webfinger", "/public.php?service=webfinger",
	); err != nil {
		return err
	}

	if err := r.reconcileReplaceRegexMiddleware(
		ctx, nc, middlewareName(nc.Name, middlewareNameHostMeta),
		"^/.well-known/host-meta", "/public.php?service=host-meta",
	); err != nil {
		return err
	}

	if err := r.reconcileReplaceRegexMiddleware(
		ctx, nc, middlewareName(nc.Name, middlewareNameHostMetaJson),
		"^/.well-known/host-meta.json", "/public.php?service=host-meta-json",
	); err != nil {
		return err
	}

	return nil
}

func (r *NextcloudReconciler) reconcileRedirectHttpsMiddleware(ctx context.Context, nc nextcloud.Nextcloud) error {
	mw := &traefik.Middleware{
		ObjectMeta: metav1.ObjectMeta{
			Name:      middlewareName(nc.Name, middlewareNameRedirectHttps),
			Namespace: nc.Namespace,
			Labels:    labelSelectors(nc),
		},
		Spec: traefik.MiddlewareSpec{
			RedirectScheme: &traefikdynamic.RedirectScheme{
				Scheme:    "https",
				Permanent: true,
			},
		},
	}

	return r.createOrUpdate(ctx, nc, mw, func() error { return nil })
}

func (r *NextcloudReconciler) reconcileReplaceRegexMiddleware(ctx context.Context, nc nextcloud.Nextcloud,
	name, regex, replacement string) error {
	mw := &traefik.Middleware{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: nc.Namespace,
			Labels:    labelSelectors(nc),
		},
		Spec: traefik.MiddlewareSpec{
			ReplacePathRegex: &traefikdynamic.ReplacePathRegex{
				Regex:       regex,
				Replacement: replacement,
			},
		},
	}

	return r.createOrUpdate(ctx, nc, mw, func() error { return nil })
}

func (r *NextcloudReconciler) reconcileIngressRoute(ctx context.Context, nc nextcloud.Nextcloud) error {
	http := ingressRoute(nc, false)

	if err := r.createOrUpdate(ctx, nc, http, func() error { return nil }); err != nil {
		return err
	}

	https := ingressRoute(nc, true)

	return r.createOrUpdate(ctx, nc, https, func() error { return nil })
}

func ingressRoute(nc nextcloud.Nextcloud, tls bool) *traefik.IngressRoute {
	route := &traefik.IngressRoute{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ingressRouteName(tls, nc.Name),
			Namespace: nc.Namespace,
			Labels:    labelSelectors(nc),
		},
		Spec: traefik.IngressRouteSpec{
			EntryPoints: []string{entrypoint(tls, nc)},
			Routes: []traefik.Route{
				{
					Match: fmt.Sprintf("Host(`%s`)", nc.Spec.Ingress.Host),
					Kind:  "Rule",
					Services: []traefik.Service{
						{LoadBalancerSpec: traefik.LoadBalancerSpec{Name: nc.Name, Port: httpPort}},
					},
					Middlewares: middlewareRefs(tls, nc.Name, nc.Namespace),
				},
			},
		},
	}

	if tls {
		route.Spec.TLS = &traefik.TLS{
			SecretName: certSecretName(nc.Name),
		}
	}

	return route
}

func middlewareRefs(tls bool, ncName, namespace string) []traefik.MiddlewareRef {
	middlewares := []traefik.MiddlewareRef{
		{Name: middlewareName(ncName, middlewareNameCaldav), Namespace: namespace},
		{Name: middlewareName(ncName, middlewareNameWebfinger), Namespace: namespace},
		{Name: middlewareName(ncName, middlewareNameHostMeta), Namespace: namespace},
		{Name: middlewareName(ncName, middlewareNameHostMetaJson), Namespace: namespace},
	}

	if !tls {
		return append(middlewares, traefik.MiddlewareRef{
			Name:      middlewareName(ncName, middlewareNameRedirectHttps),
			Namespace: namespace,
		})
	}

	return middlewares
}

func ingressRouteName(tls bool, ncName string) string {
	if tls {
		return fmt.Sprintf("%s-%s", ncName, "https")
	}
	return fmt.Sprintf("%s-%s", ncName, "http")
}

func entrypoint(tls bool, nc nextcloud.Nextcloud) string {
	if tls {
		return nc.Spec.Ingress.Traefik.HttpsEntrypoint
	}
	return nc.Spec.Ingress.Traefik.HttpEntrypoint
}

func middlewareName(ncName, mwName string) string {
	return fmt.Sprintf("%s-%s", ncName, mwName)
}

func certSecretName(ncName string) string {
	return fmt.Sprintf("%s%s", ncName, certSecretSuffix)
}
