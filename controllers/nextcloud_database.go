package controllers

import (
	"context"
	"fmt"
	"github.com/go-logr/logr"
	postgres "github.com/zalando/postgres-operator/pkg/apis/acid.zalan.do"
	postgresv1 "github.com/zalando/postgres-operator/pkg/apis/acid.zalan.do/v1"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"strings"
)

func (r *NextcloudReconciler) reconcileDatabase(ctx context.Context,
	nc nextcloud.Nextcloud) (nextcloud.Nextcloud, error) {

	var database postgresv1.Postgresql
	if err := r.Get(ctx,
		types.NamespacedName{Name: databaseServiceName(nc.Name), Namespace: nc.Namespace}, &database); err != nil {
		if apierrors.IsNotFound(err) {
			logr.FromContext(ctx).Info("create postgres database")
			return r.createPostgres(ctx, nc)
		}
		return nextcloud.DatabaseFailed(nc, nextcloud.DeploymentFailedReason, err.Error()), err
	}

	return r.checkDatabaseStatus(ctx, nc)

}

func (r *NextcloudReconciler) createPostgres(ctx context.Context, nc nextcloud.Nextcloud) (nextcloud.Nextcloud, error) {
	ls := labelSelectors(nc)
	ls[postgresTeamLabel] = nc.Name

	database := &postgresv1.Postgresql{
		ObjectMeta: metav1.ObjectMeta{
			Name:      databaseServiceName(nc.Name),
			Namespace: nc.Namespace,
			Labels:    ls,
		},
		Spec: r.createPostgresSpec(nc),
	}

	if err := ctrl.SetControllerReference(&nc, database, r.Scheme); err != nil {
		return nextcloud.DatabaseFailed(nc, nextcloud.DatabaseFailedReason, err.Error()), err
	}

	if err := r.Create(ctx, database); err != nil {
		return nextcloud.DatabaseFailed(nc, nextcloud.DatabaseFailedReason, err.Error()), err
	}

	return nextcloud.DatabaseProgressing(nc, "Database created"), nil
}

func (r *NextcloudReconciler) createPostgresSpec(nc nextcloud.Nextcloud) postgresv1.PostgresSpec {
	return postgresv1.PostgresSpec{
		TeamID: nc.Name,
		Volume: postgresv1.Volume{
			Size:         nc.Spec.Database.VolumeSize,
			StorageClass: nc.Spec.Database.StorageClass,
		},
		NumberOfInstances: nc.Spec.Database.Replicas,
		Users: map[string]postgresv1.UserFlags{
			superuser:             []string{superuserFlag, createdbFlag},
			databaseUser(nc.Name): []string{},
		},
		Databases: map[string]string{databaseName: databaseName},
		PostgresqlParam: postgresv1.PostgresqlParam{
			PgVersion: pgVersion,
		},
		SpiloRunAsGroup: newInt64(spiloRunAsGroup),
		SpiloFSGroup:    newInt64(spiloFSGroup),
		SpiloRunAsUser:  newInt64(spiloRunAsGroup),

		Resources: postgresv1.Resources{
			ResourceRequests: postgresv1.ResourceDescription{
				CPU:    "100m",
				Memory: "100Mi",
			},
			ResourceLimits: postgresv1.ResourceDescription{
				CPU:    "1",
				Memory: "500Mi",
			},
		},

		Sidecars: []postgresv1.Sidecar{
			{
				Name:        exporterName,
				DockerImage: nc.Spec.Database.ExporterImage,
				Resources: postgresv1.Resources{
					ResourceRequests: postgresv1.ResourceDescription{
						CPU:    "100m",
						Memory: "200M",
					},
					ResourceLimits: postgresv1.ResourceDescription{
						CPU:    "500m",
						Memory: "256M",
					},
				},
				Env: []corev1.EnvVar{
					{Name: exporterDataSourceURIEnv, Value: exporterDataSourceURI},
					{
						Name: exporterDataSourceUserEnv,
						ValueFrom: &corev1.EnvVarSource{
							SecretKeyRef: &corev1.SecretKeySelector{
								LocalObjectReference: corev1.LocalObjectReference{
									Name: secretName(postgresUser, nc.Name),
								},
								Key:      usernameSecretKey,
								Optional: newBool(false),
							},
						},
					},
					{
						Name: exporterDataSourcePassEnv,
						ValueFrom: &corev1.EnvVarSource{
							SecretKeyRef: &corev1.SecretKeySelector{
								LocalObjectReference: corev1.LocalObjectReference{
									Name: secretName(postgresUser, nc.Name),
								},
								Key:      passwordSecretKey,
								Optional: newBool(false),
							},
						},
					},
				},
			},
		},
	}
}

func (r *NextcloudReconciler) checkDatabaseStatus(ctx context.Context,
	nc nextcloud.Nextcloud) (nextcloud.Nextcloud, error) {

	log := logr.FromContext(ctx)

	postgresObjectKey := client.ObjectKey{
		Name:      databaseServiceName(nc.Name),
		Namespace: nc.Namespace,
	}

	var database postgresv1.Postgresql
	if err := r.Get(ctx, postgresObjectKey, &database); err != nil {
		return nextcloud.DatabaseFailed(nc, nextcloud.DatabaseFailedReason, err.Error()), err
	}

	if database.Status.PostgresClusterStatus == postgresv1.ClusterStatusRunning {
		log.Info("database ready")
		return nextcloud.DatabaseReady(nc, nextcloud.DatabaseSucceededReason, "Database ready"), nil
	} else if database.Status.PostgresClusterStatus == postgresv1.ClusterStatusCreating ||
		database.Status.PostgresClusterStatus == postgresv1.ClusterStatusUpdating {
		log.Info("database progressing")
		return nextcloud.DatabaseProgressing(nc, database.Status.PostgresClusterStatus), nil
	} else if database.Status.PostgresClusterStatus == "" {
		log.Info("database progressing")
		return nextcloud.DatabaseProgressing(nc, "Unknown"), nil
	}

	return nextcloud.DatabaseFailed(nc, nextcloud.DatabaseFailedReason, database.Status.PostgresClusterStatus),
		fmt.Errorf(database.Status.PostgresClusterStatus)
}

func databaseServiceName(ncName string) string {
	return fmt.Sprintf("%s-pg", ncName)
}

func databaseUser(ncName string) string {
	return fmt.Sprintf("%s_user", ncName)
}

func secretName(username, ncName string) string {
	return fmt.Sprintf("%s.%s.credentials.%s.%s",
		strings.ReplaceAll(username, "_", "-"),
		databaseServiceName(ncName),
		postgresv1.PostgresCRDResourceKind,
		postgres.GroupName,
	)
}
