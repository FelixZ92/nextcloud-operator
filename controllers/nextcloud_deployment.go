package controllers

import (
	"context"
	"fmt"
	nextcloud "gitlab.com/felixz92/nextcloud-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (r *NextcloudReconciler) reconcileDeployment(ctx context.Context, nc nextcloud.Nextcloud) (appsv1.Deployment, error) {
	ls := labelSelectors(nc)
	deploy := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      nc.Name,
			Namespace: nc.Namespace,
			Labels:    ls,
		},
		Spec: appsv1.DeploymentSpec{
			ProgressDeadlineSeconds: newInt32(progressDeadLineSeconds),
			Replicas:                replicas(nc.Spec.Replicas),
			Strategy:                appsv1.DeploymentStrategy{Type: appsv1.RollingUpdateDeploymentStrategyType},
			RevisionHistoryLimit:    newInt32(revisionHistoryLimit),
		},
	}

	if err := r.createOrUpdate(ctx, nc, deploy, func() error {
		if deploy.ObjectMeta.CreationTimestamp.IsZero() {
			deploy.Spec.Selector = metav1.SetAsLabelSelector(ls)
		}
		deploy.Spec.Template = corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: ls,
			},
			Spec: corev1.PodSpec{
				ServiceAccountName: installationResourceName(nc.Name),
				Containers: []corev1.Container{
					createRuntimeContainer(nc),
					createNginxContainer(nc),
				},
				Volumes: []corev1.Volume{
					createWebrootVolume(nc),
					createEmptyDirVolume(dataVolumeName, "100Mi"),
					createConfigVolume(nc),
					createNginxConfigVolume(nc.Name),
					createEmptyDirVolume("nginx", "1Mi"),
				},
				SecurityContext: &corev1.PodSecurityContext{
					FSGroup:   newInt64(nextcloudFSGroup),
					RunAsUser: newInt64(nextcloudFSGroup),
				},
			},
		}

		return nil
	}); err != nil {
		return *deploy, err
	}

	return *deploy, nil
}

func (r *NextcloudReconciler) checkHealth(deployment appsv1.Deployment) error {
	availableCondition := getDeploymentCondition(deployment.Status, appsv1.DeploymentAvailable)

	err := r.checkAvailability(availableCondition, deployment)
	if err != nil {
		return err
	}

	if reason, ok := deploymentProgressFailed(&deployment); ok {
		return fmt.Errorf(reason)
	}

	return nil
}

func (r *NextcloudReconciler) checkAvailability(availableCondition *appsv1.DeploymentCondition,
	deployment appsv1.Deployment) error {
	if availableCondition == nil {
		return fmt.Errorf("deployment not available")
	}

	if availableCondition.Status == corev1.ConditionFalse || availableCondition.Status == corev1.ConditionUnknown {
		if reason, ok := deploymentProgressFailed(&deployment); ok {
			return fmt.Errorf(reason)
		}
	}

	if message, ok := deploymentProgressFailed(&deployment); ok {
		return fmt.Errorf(message)
	}

	return nil
}

func (r *NextcloudReconciler) checkProgressing(deployment appsv1.Deployment, nc nextcloud.Nextcloud) bool {
	desiredReplicas := nc.Spec.Replicas
	actualStatus := deployment.Status
	//  nolint:godox @todo i think this is nonsense
	return actualStatus.ReadyReplicas != desiredReplicas ||
		actualStatus.Replicas != desiredReplicas ||
		actualStatus.UpdatedReplicas != desiredReplicas ||
		actualStatus.AvailableReplicas != desiredReplicas ||
		actualStatus.UnavailableReplicas != 0
}

func replicas(replicas int32) *int32 {
	var r int32
	if replicas == 0 {
		r = 1
	} else {
		r = replicas
	}

	return &r
}
