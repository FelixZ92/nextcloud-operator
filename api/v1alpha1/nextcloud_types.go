/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	cmmeta "github.com/jetstack/cert-manager/pkg/apis/meta/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	NextcloudFinalizer = "finalizers.nextcloud.zippelf.com"

	ApplicationName = "nextcloud"

	NameLabel           = "app.kubernetes.io/name"
	InstanceLabel       = "app.kubernetes.io/instance"
	ManagedByLabel      = "app.kubernetes.io/managed-by"
	OwnerLabel          = "app.kubernetes.io/owner"
	OwnerNamespaceLabel = "app.kubernetes.io/owner-namespace"
	VersionLabel        = "app.kubernetes.io/version"
)

type NextcloudAdminUserSpec struct {
	Username *corev1.SecretKeySelector `json:"username,omitempty" protobuf:"bytes,4,opt,name=username"`
	Password *corev1.SecretKeySelector `json:"password,omitempty" protobuf:"bytes,4,opt,name=password"`
}

type NextcloudDatabase struct {
	// +kubebuilder:default:=false
	Enabled bool `json:"enabled,omitempty"`
	// +kubebuilder:default:=1
	Replicas int32 `json:"replicas,omitempty"`
	// +kubebuilder:default:="wrouesnel/postgres_exporter:v0.8.0"
	ExporterImage string `json:"exporterImage,omitempty"`
	// +kubebuilder:default:="2Gi"
	VolumeSize string `json:"volumeSize,omitempty"`
	// +kubebuilder:default:=default
	StorageClass string `json:"storageClass,omitempty"`
}

type SwiftObjectStorage struct {
	Url string `json:"url"`
	// +kubebuilder:default:=false
	Autocreate   bool                      `json:"autocreate"`
	UserName     *corev1.SecretKeySelector `json:"username"`
	UserPassword *corev1.SecretKeySelector `json:"userPassword"`
	// +kubebuilder:default:=Default
	UserDomain  string                    `json:"userDomain,omitempty"`
	ProjectName *corev1.SecretKeySelector `json:"projectName,omitempty"`
	// +kubebuilder:default:=Default
	ProjectDomain string `json:"projectDomain,omitempty"`
	// +kubebuilder:default:=swift
	ServiceName   string `json:"serviceName,omitempty"`
	Region        string `json:"region,omitempty"`
	ContainerName string `json:"containerName,omitempty"`
}

type NextcloudObjectStorage struct {
	SwiftObjectStorage SwiftObjectStorage `json:"swift,omitempty"`
}

type NextcloudNginxSpec struct {
	// +kubebuilder:default:="nginx:alpine"
	Image string `json:"image,omitempty"`

	Resources corev1.ResourceRequirements `json:"resources,omitempty"`
}

type NextcloudCertificate struct {
	Issuer cmmeta.ObjectReference `json:"issuer"`
}

type NextcloudTraefik struct {
	// +kubebuilder:default:=web
	HttpEntrypoint string `json:"httpEntrypoint"`
	// +kubebuilder:default:=websecure
	HttpsEntrypoint string `json:"httpsEntrypoint"`
}

type NextcloudIngressSpec struct {
	Host string `json:"host"`

	Certificate NextcloudCertificate `json:"certificate"`

	Traefik NextcloudTraefik `json:"traefik"`
}

type NextcloudPersistence struct {
	WebrootVolume corev1.PersistentVolumeClaimSpec `json:"webroot"`
	ConfigVolume  corev1.PersistentVolumeClaimSpec `json:"config"`
}

// NextcloudSpec defines the desired state of Nextcloud
type NextcloudSpec struct {
	// +kubebuilder:default:="registry.gitlab.com/felixz92/nextcloud-operator/nextcloud-install:20.0.12"
	InstallationImage string `json:"installationImage,omitempty"`

	// +kubebuilder:default:="registry.gitlab.com/felixz92/nextcloud-operator/php-fpm:7.4"
	RuntimeImage string `json:"runtimeImage,omitempty"`

	Replicas int32 `json:"replicas,omitempty"`

	AdminUser NextcloudAdminUserSpec `json:"adminUser"`

	// +kubebuilder:default:=/nextcloud-data
	DataDir string `json:"dataDir,omitempty"`

	Database NextcloudDatabase `json:"database,omitempty"`

	ObjectStorage NextcloudObjectStorage `json:"objectStorage,omitempty"`

	Resources corev1.ResourceRequirements `json:"resources,omitempty"`

	NginxSpec NextcloudNginxSpec `json:"nginx,omitempty"`

	Ingress NextcloudIngressSpec `json:"ingress"`

	Persistence NextcloudPersistence `json:"persistence"`
}

// NextcloudStatus defines the observed state of Nextcloud
type NextcloudStatus struct {
	// +optional
	Conditions []metav1.Condition `json:"conditions,omitempty"`
	Replicas   int32              `json:"replicas,omitempty"`
	Selector   string             `json:"selector,omitempty"`
}

//+kubebuilder:object:root=true
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas,selectorpath=.status.selector
// +kubebuilder:printcolumn:name="Healthy",type="string",JSONPath=".status.conditions[?(@.type==\"Healthy\")].status",description=""
// +kubebuilder:printcolumn:name="Status",type="string",JSONPath=".status.conditions[?(@.type==\"Healthy\")].message",description=""
// +kubebuilder:printcolumn:name="Installation",type="string",JSONPath=".status.conditions[?(@.type==\"Installed\")].message",description=""
// +kubebuilder:printcolumn:name="Database",type="string",JSONPath=".status.conditions[?(@.type==\"DatabaseReady\")].message",description=""
// +kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description=""

// Nextcloud is the Schema for the nextclouds API
type Nextcloud struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   NextcloudSpec   `json:"spec,omitempty"`
	Status NextcloudStatus `json:"status,omitempty"`
}

func DatabaseReady(n Nextcloud, reason, message string) Nextcloud {
	SetDatabaseReadyCondition(&n, metav1.ConditionTrue, reason, message)
	return n
}

func DatabaseProgressing(n Nextcloud, message string) Nextcloud {
	SetDatabaseReadyCondition(&n, metav1.ConditionUnknown, DatabaseProgressingReason, message)
	return n
}

func DatabaseFailed(n Nextcloud, reason, message string) Nextcloud {
	SetDatabaseReadyCondition(&n, metav1.ConditionFalse, reason, message)
	return n
}

func SetDatabaseReadyCondition(n *Nextcloud, status metav1.ConditionStatus, reason, message string) {
	SetResourceCondition(n, DatabaseReadyCondition, status, reason, message)
}

func NextcloudInstalled(n Nextcloud, reason, message string) Nextcloud {
	SetInstalledCondition(&n, metav1.ConditionTrue, reason, message)
	return n
}

func NextcloudInstalling(n Nextcloud) Nextcloud {
	SetInstalledCondition(&n, metav1.ConditionUnknown, InstallationProgressingReason, "Nextcloud installing")
	return n
}

func NextcloudInstallFailed(n Nextcloud, reason, message string) Nextcloud {
	SetInstalledCondition(&n, metav1.ConditionFalse, reason, message)
	return n
}

func SetInstalledCondition(n *Nextcloud, status metav1.ConditionStatus, reason, message string) {
	SetResourceCondition(n, InstalledCondition, status, reason, message)
}

func NextcloudHealthy(n Nextcloud) Nextcloud {
	SetHealthyCondition(&n, metav1.ConditionTrue, ReconciliationSucceededReason, "Nextcloud healthy")
	return n
}

func NextcloudNotHealthy(n Nextcloud, reason, message string) Nextcloud {
	SetHealthyCondition(&n, metav1.ConditionFalse, reason, message)
	return n
}

func SetHealthyCondition(n *Nextcloud, status metav1.ConditionStatus, reason, message string) {
	SetResourceCondition(n, HealthyCondition, status, reason, message)
}

// GetStatusConditions returns a pointer to the Status.Conditions slice
func (in *Nextcloud) GetStatusConditions() *[]metav1.Condition {
	return &in.Status.Conditions
}

func (in *Nextcloud) DatabaseReady() bool {
	return hasConditionWithStatus(in.Status.Conditions, DatabaseReadyCondition, metav1.ConditionTrue)
}

func (in *Nextcloud) DatabaseNotReady() bool {
	return !in.DatabaseReady()
}

func (in *Nextcloud) IsInstalled() bool {
	return hasConditionWithStatus(in.Status.Conditions, InstalledCondition, metav1.ConditionTrue)
}

func (in *Nextcloud) IsNotInstalled() bool {
	return !in.IsInstalled()
}

func (in *Nextcloud) IsHealthy() bool {
	return hasConditionWithStatus(in.Status.Conditions, HealthyCondition, metav1.ConditionTrue)
}

func (in *Nextcloud) IsNotHealthy() bool {
	return !in.IsHealthy()
}

//+kubebuilder:object:root=true

// NextcloudList contains a list of Nextcloud
type NextcloudList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Nextcloud `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Nextcloud{}, &NextcloudList{})
}
