package v1alpha1

import (
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	DatabaseReadyCondition = "DatabaseReady"
	InstalledCondition     = "Installed"
	HealthyCondition       = "Healthy"
)

const (
	DatabaseProgressingReason = "DatabaseProgressing"
	DatabaseFailedReason      = "DatabaseFailed"
	DatabaseSucceededReason   = "DatabaseSucceeded"

	InstallationProgressingReason = "InstallationProgressing"
	InstallationFailedReason      = "InstallationFailed"
	InstallationSucceededReason   = "InstallationSucceeded"

	// DeploymentFailedReason represents the fact that the
	// creation or update of Nextcloud Deployment failed.
	DeploymentFailedReason string = "DeploymentFailed"

	// ConfigMapFailedReason represents the fact that the
	// creation or update of Nextcloud ConfigMap failed.
	ConfigMapFailedReason string = "ConfigMapFailed"

	// ServiceFailedReason represents the fact that the
	// creation or update of Nextcloud Service failed.
	ServiceFailedReason string = "ServiceFailed"

	// IngressFailedReason represents the fact that the
	// creation or update of Nextcloud Ingress failed.
	IngressFailedReason string = "IngressFailed"

	// HealthCheckFailedReason represents the fact that
	// one of the health checks of the Deployment failed.
	HealthCheckFailedReason string = "HealthCheckFailed"

	// ReconciliationSucceededReason represents the fact that the reconciliation of a resource has succeeded.
	ReconciliationSucceededReason string = "ReconciliationSucceeded"

	// TimedOutReason is added in a deployment when its newest replica set fails to show any progress
	// within the given deadline (progressDeadlineSeconds).
	TimedOutReason = "ProgressDeadlineExceeded"

	// FailedRSCreateReason is added in a deployment when it cannot create a new replica set.
	FailedRSCreateReason = "ReplicaSetCreateError"
)

// ObjectWithStatusConditions is an interface that describes kubernetes resource
// type structs with Status Conditions
// +k8s:deepcopy-gen=false
type ObjectWithStatusConditions interface {
	GetStatusConditions() *[]metav1.Condition
}

// SetResourceCondition sets the given condition with the given status,
// reason and message on a resource.
func SetResourceCondition(obj ObjectWithStatusConditions, condition string, status metav1.ConditionStatus, reason, message string) {
	conditions := obj.GetStatusConditions()

	newCondition := metav1.Condition{
		Type:    condition,
		Status:  status,
		Reason:  reason,
		Message: message,
	}

	apimeta.SetStatusCondition(conditions, newCondition)
}

func hasConditionWithStatus(conditions []metav1.Condition, conditionType string, status metav1.ConditionStatus) bool {
	for _, c := range conditions {
		if c.Type == conditionType {
			if c.Status == status {
				return true
			}
			return false
		}
	}
	return false
}
