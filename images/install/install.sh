#!/usr/bin/env sh

set -eux

directory_empty() {
    [ -z "$(ls -A "$1/")" ]
}

if [ -f "/var/www/html/version.php" ] && grep 'installed' "/var/www/html/config/config.php" | grep -q 'true' ; then
  echo "Already installed. Upgrade or downgrade not supported."
  exit 1
fi

rsync -rlD --delete --exclude-from=/upgrade.exclude /data/nextcloud/ /var/www/html/
for dir in config data custom_apps themes; do
    if [ ! -d "/var/www/html/$dir" ] || directory_empty "/var/www/html/$dir"; then
        rsync -rlD --include "/$dir/" --exclude '/*' /data/nextcloud/ /var/www/html/
    fi
done
rsync -rlD --include "/version.php" --exclude '/*' /data/nextcloud/ /var/www/html/
echo "Successfully copied files"

install_options='-n --admin-user "$NEXTCLOUD_ADMIN_USER" --admin-pass "$NEXTCLOUD_ADMIN_PASSWORD"'
install_options=$install_options' --data-dir "$NEXTCLOUD_DATA_DIR"'
#install_options=$install_options' --database-name "$SQLITE_DATABASE"'
install_options=$install_options' --database pgsql --database-name "$POSTGRES_DB" --database-user "$POSTGRES_USER" --database-pass "$POSTGRES_PASSWORD" --database-host "$POSTGRES_HOST"'

echo "starting nextcloud installation"
max_retries=10
try=0
until sh -c "php /var/www/html/occ maintenance:install $install_options" || [ "$try" -gt "$max_retries" ]
do
    echo "retrying install..."
    try=$((try+1))
    sleep 10s
done
if [ "$try" -gt "$max_retries" ]; then
    echo "installing of nextcloud failed!"
    exit 1
fi

if [ -n "${NEXTCLOUD_TRUSTED_DOMAINS+x}" ]; then
  echo "setting trusted domains…"
  NC_TRUSTED_DOMAIN_IDX=1
  for DOMAIN in $NEXTCLOUD_TRUSTED_DOMAINS ; do
      DOMAIN=$(echo "$DOMAIN" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')
      sh -c "php /var/www/html/occ config:system:set trusted_domains $NC_TRUSTED_DOMAIN_IDX --value=$DOMAIN"
      NC_TRUSTED_DOMAIN_IDX=$(($NC_TRUSTED_DOMAIN_IDX+1))
  done
fi

echo "All done."
