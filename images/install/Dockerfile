FROM registry.gitlab.com/felixz92/nextcloud-operator/php-fpm:7.4

ENV NEXTCLOUD_VERSION 20.0.12

USER root

RUN apk add --no-cache rsync

RUN apk add --no-cache --virtual .fetch-deps bzip2 gnupg \
    && mkdir -p /data \
    && chown www-data:www-data /data \
    && curl -fsSL -o /tmp/nextcloud.tar.bz2 \
        "https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2" \
    && curl -fsSL -o /tmp/nextcloud.tar.bz2.asc \
        "https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.asc" \
    && export GNUPGHOME="$(mktemp -d)" \
    && curl -fsSL -o /tmp/nextcloud.asc \
        "https://nextcloud.com/nextcloud.asc" \
    && gpg --batch --import /tmp/nextcloud.asc \
    && gpg --batch --verify /tmp/nextcloud.tar.bz2.asc /tmp/nextcloud.tar.bz2 \
    && tar -xjf /tmp/nextcloud.tar.bz2 -C /data/ \
    && gpgconf --kill all \
    && rm /tmp/nextcloud.tar.bz2 /tmp/nextcloud.tar.bz2.asc \
    && rm -rf "$GNUPGHOME" /data/nextcloud/updater \
    && mkdir -p /data/nextcloud/data \
    && mkdir -p /data/nextcloud/custom_apps \
    && chmod +x /data/nextcloud/occ \
    && chown -R www-data:www-data /data/nextcloud \
    && apk del .fetch-deps

COPY *.sh upgrade.exclude /
COPY config/* /data/nextcloud/config/

USER 82

ENTRYPOINT ["/install.sh"]
